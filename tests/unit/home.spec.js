import Home from '@/views/Home.vue'

describe('Home.vue', () => {
  it('Home instance', () => {
    expect(typeof Home.data).toBe('function')
  })

  it('Home default data', () => {
    expect(typeof Home.data).toBe('function')
    const defaultData = Home.data()
    expect(defaultData.options).toEqual(expect.arrayContaining([
      {
        path: "/video",
        image: "video.png",
        maxWidth: "174",
        maxHeight: "107",
      },
      {
        path: "/prospects",
        image: "conocer.png",
        maxWidth: "330",
        maxHeight: "107",
      },
    ]))
  })
})
