# CHALLENGE CABIFY

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.

### Información 📋
```
VUE: ^2.6.11
VUETIFY: ^2.4.0
```

### Instalación 🔧
_Clonar_
```
git clone git@gitlab.com:challenges9/zarego/challenge.git
cd challenge
```

_Instalar Dependencias_
```
yarn install
```

_Configurar variable de entorno local_
```
 cp .env.local.example .env.local
```

_Iniciar servidor local(puerto:8000)_
```
yarn serve
En el navegador: http://localhost:8000
```

## Autores ✒️
* **Alex Velasquez**
