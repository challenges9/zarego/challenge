import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Video from '../views/Video'
import Prospect from '../views/Prospect'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/video',
    name: 'Video',
    component: Video
  },
  {
    path: '/prospects',
    name: 'Prospect',
    component: Prospect
  },
]

const router = new VueRouter({
  routes
})

export default router
