import axios from 'axios'
class HttpClient {
    /** singleton */
    static getInstance() {
        if (HttpClient.instance == undefined) {
            HttpClient.instance = new this();
        }
        return HttpClient.instance
    }

    constructor() {
        this.axiosInstance = axios.create()
    }

    get(path) {
        return this.axiosInstance.get(path)
    }

    post(path, data) {
        return this.axiosInstance.post(path, data)
    }

}
export { HttpClient }