import { HttpClient } from '../utils/HttpClient'
const http = HttpClient.getInstance()

export default {
  createProspect: (prospect) => {
    return http.post(`api/prospects`, prospect);
  },
}