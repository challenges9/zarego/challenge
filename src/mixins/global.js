export default {
    methods: {
        getAlert(code, msg) {
            return {
                type: this.typeStatusCode[code],
                msg: msg
            }
        }
    },
    computed: {
        pathImg() {
            return process.env.VUE_APP_PATH_IMG;
        },
        pathVimeo() {
            return process.env.VUE_APP_PATH_VIMEO;
        },
        typeStatusCode() {
            return {
                400: "orange lighten-3",
                401: "orange lighten-3",
                500: "red lighten-2"
            }
        }
    }
};