module.exports = {
  devServer: {
    port: 8000,
    proxy: {
      '^/api/': {
        target: process.env.VUE_APP_API,
        ws: true,
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          "/api/*": "/"
        } 
      },
    },
  },
  transpileDependencies: [
    'vuetify'
  ],
}
